<?php

defined('BASEPATH') OR exit('No direct script access allowed');

$this->load->view('template/head');
?>
<p>&nbsp;<br>&nbsp;<p>
<div class="container">
	<div  class="row">
		<div class="col-md-12">
			<center>
		        <strong><?php echo $this->session->flashdata('notif'); ?></strong>
			</center>

			<?php echo form_open_multipart('welcome/import', ['class'=>'form-horizontal']); ?>
			<div class="col-md-6"><input type="file" name="userfile" id="userfile"></div>
			<div class="col-md-6"><input type="submit" name="tambah" class="btn btn-sm btn-info"></div>&nbsp;
			
			
			<?php form_close(); ?>

			<table class="table table-hover">
				<thead style="background-color:#ff0000;color:white">
					<tr>
						<td>No.</td>
						<td>Nama File</td>
						<td>Simple Text</td>
						<td>Action</td>
					</tr>
				</thead>
				<tbody>
					<?php $a=0; foreach ($dokumen as $doc) { ?>
						<tr>
							<td><?php $a++; echo $a; ?></td>
							<td><?php echo $doc->nama; ?></td>
							<td><?php echo substr($doc->dokumen, 0, 60); ?></td>
							<td><?php echo anchor('welcome/view/'.$doc->id, 'VIEW', ['class'=>'btn btn-sm btn-info']); ?>&nbsp;
							<?php echo anchor('welcome/hapus/'.$doc->id, 'HAPUS', ['class'=>'btn btn-sm btn-danger',
							'onclick'=>'return confirm(\'APAKAH ANDA INGIN MENGHAPUS ?\')']); ?>&nbsp;
								<?php echo anchor('welcome/token/'.$doc->id, 'TOKEN',['class'=>'btn btn-sm btn-success']); ?>
							</td>
						</tr>
					<?php } ?>
				</tbody>
			</table>
		</div>
	</div>
</div>
<?php $this->load->view('template/foot'); ?>