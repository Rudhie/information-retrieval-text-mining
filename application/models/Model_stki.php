<?php
	defined('BASEPATH') OR exit ('No Script Access Allowed');

	//class model bernama model_stki dengan mendapat turunan dari class ci model
	class Model_stki extends CI_Model{

		//fungsi untuk memanggil seluruh isi
		function view_all(){

			//memanggil atau menampilkan (select) data dari table isi
			$hasil = $this->db->get('isi');

			//pengecekan kondisi jika hasil lebih dari nol
			if($hasil->num_rows() > 0){

				//mengembalikan nilai result yakni isi dari database
				return $hasil->result();

			//jika tidak (kurang dari nol)
			} else {

				//mengembalikan array dengan nilai kosong
				return array();
			}
		}

		//fungsi untuk menambhkan data kedalam db
		function add($isi){

			//query untuk menyimpan data ke db dengan paramater yang didapatkan dari controller
			$this->db->insert('isi',$isi);
		}

		//fungsi untuk menampilkan hasil import
		function lihat($id){
			//perintah untuk menampilkan (select) dari table berdasarkan id
			$tes = $this->db->where('id', $id)->get('isi');
			//memberikan nilai kembali berupa hasil
			return $tes->result();
		}

		//fungsi untuk tokenisasi
		function tokenisasi($id){
			//perintah untuk menampilkan (select) dari table berdasarkan id
			$tk = $this->db->where('id', $id)->limit(1)->get('isi');
			if($tk->num_rows() > 0){
				return $tk->result();
			} else {
				return array();
			}
		}

		//fungsi untuk update hasil tokenisasi
		function update_token($where, $token){
			 $this->db->where($where);
			 $this->db->update('isi', $token);
		}

		//fungsi untuk hapus
		function hapus($id){
			$this->db->where('id', $id)->delete('isi');
		}

		//fungsi untuk mengubah
		function edit($id){
			$ed = $this->db->where('id', $id)->get('isi');

			if($ed->num_rows() > 0){
				return $ed->result();
			} else {
				return array();
			}
		}

		function ubah($where, $ubah_doc){
			$this->db->where($where);
			$this->db->update('isi', $ubah_doc);
		}

		function lihat_token(){
			//perintah untuk menampilkan (select) dari table berdasarkan id
			$tes = $this->db->get('isi');
			//memberikan nilai kembali berupa hasil
			return $tes->result();
		}

		function detail_token($id){
			//perintah untuk menampilkan (select) dari table berdasarkan id
			$tes = $this->db->where('id', $id)->get('isi');
			//memberikan nilai kembali berupa hasil
			return $tes->result();
		}

		function cek_token($id){
			$hasil = $this->db->where('id', $id)->get('isi');
			return $hasil->row();
		}

		function update_stem($tambah, $id){
			$this->db->where('id', $id)->update('isi', $tambah);
		}

		function cekKamus($kata){
			$hasil = $this->db->where('word', $kata)->get('dictionary');
			return $hasil->row();
		}

		function stopword($id){
			$hasil = $this->db->where('id',$id)->get('isi');
			if($hasil->num_rows() > 0){
				return $hasil->result_array();
			} else {
				return array();
			}
		}

		function simpan_stopword($id, $simpan_stopword){
			$this->db->where('id', $id);
			$this->db->update('isi', $simpan_stopword);
		}

		function simpan_stemming($id, $kirim){
			$this->db->where('id', $id);
			$this->db->update('isi', $kirim);	
		}

		function hasil_stem(){
			$hasil = $this->db->get('isi');
			if($hasil->num_rows() > 0){
				return $hasil->result();
			} else {
				array();
			}
		}

		function detail_stem($id){
			$hasil = $this->db->where('id', $id)->get('isi');
			if($hasil->num_rows() > 0){
				return $hasil->result();
			} else {
				array();
			}
		}

		function simpan_index($id, $simpan){
			$this->db->where('id', $id);
			$this->db->update('isi', $simpan);
		}

		function ambil_keyword($construct){
			$hasil = $this->db->query("SELECT distinct * FROM isi WHERE".$construct);
			if($hasil->num_rows() > 0){
				return $hasil->row();
			} else{
				return array();
			}
		}

	}
?>

