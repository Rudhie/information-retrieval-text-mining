<?php
defined('BASEPATH') OR exit('No direct script access allowed');
define("DOC_ID",0);
define("TERM_POSITION",1);

class Bobot extends CI_Controller {

	public $num_docs = 0;
	public $corpus_terms = array();

	function show_docs($doc) {
		$jumlah_doc = count($doc);
		for($i=0; $i < $jumlah_doc; $i++) {
			echo "Dokumen ke-$i : ".$doc[$i];
		}
	}

	function create_index($D){
		$this->num_docs = count($D);
		for ($doc_num=0; $doc_num < $this->num_docs; $doc_num++) { 
			$doc_terms = array();
			$doc_terms = explode(" ", $D[$doc_num]);

			$num_terms = count($doc_terms);
			for ($term_position=0; $term_position < $num_terms; $term_position++) { 
				$term = strtolower($doc_terms[$term_position]);
				$this->corpus_terms[$term][] = array($doc_num, $term_position);
			}
		}
	}

	function show_index() {
		ksort($this->corpus_terms);

		foreach($this->corpus_terms AS $term => $doc_locations) {
			echo "<b>$term:</b> ";
			foreach($doc_locations AS $doc_location)
				echo "{".$doc_location[DOC_ID].", ".$doc_location[TERM_POSITION]."} ";
			echo "<br />";
		}
	}

	function tf($term) {
		$term = strtolower($term);
		return count($this->corpus_terms[$term]);
	}

	function ndw($term) {
		$term = strtolower($term);
		$doc_locations = $this->corpus_terms[$term];
		$num_locations = count($doc_locations);
		$docs_with_term = array();
		for($doc_location=0; $doc_location < $num_locations; $doc_location++)
			$docs_with_term[$i]++;
		return count($docs_with_term);
	}

	function idf($term) {
		return log($this->num_docs)/$this->ndw($term);
	}

	function hasil(){
		$D[0] = "Membuka blog khozaimi asyik";
		$D[1] = "Selain asyik blog khozaimi juga banyak yang bisa diperoleh";
		$D[2] = "ayuk kita kunjungi blogna khozaimi biar asyik";

		echo "<p><b>Corpus:</b></p>";
		$this->show_docs($D);

		$this->create_index($D);

		echo "<p><b>Inverted Index:</b></p>";
		$this->show_index();

		$term = "asyik";  //kata asyik yang akan menjadi pusat perhitungan kita
		$tf  = $this->tf($term);
		$ndw = $this->ndw($term);
		$idf = $this->idf($term);
		echo "<p>";
		echo "Term Frequency of '$term' is $tf<br />";
		echo "Number Of Documents with $term is $ndw<br />";
		echo "Inverse Document Frequency of $term is $idf";
		echo "</p>";
	}

	function __construct(){
		parent::__construct();
		$this->load->model('Model_stki');
	}

	public function index(){
		$this->load->view('pembobotan');
	}

	public function proses(){
		$this->load->view('template/head');
		echo "<p>&nbsp;</p>&nbsp;</p>&nbsp;";
		$keyword = $this->input->post('keyword');

		$keyword = explode(' ', $keyword);

		$x=0;

		foreach ($keyword as $key => $search) {
			$x++;
			if($x==1)
				$construct .= " kata_indexing LIKE '%$search%'";
			else 
				$construct .= " OR kata_indexing LIKE '%$search%'";
			
		}

		print_r($keyword);
		echo "<hr/>";

		$ambil_keyword =$this->db->query("SELECT distinct * FROM isi WHERE".$construct)	;	

			foreach ($ambil_keyword->result() as $key => $value) {
				$id_yang_ada = $value->id;
				$nama = $value->nama;
				$isi = substr($value->dokumen, 0, 60);
				$text = $value->kata_indexing;
				$hit = str_replace($keyword, "", $text, $count);
				echo "<br/>ID ".$id_yang_ada." ada ".$count."<br/>".$isi."<hr/>";
			}
	}
}
?>