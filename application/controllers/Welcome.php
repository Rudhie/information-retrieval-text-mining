<?php
defined('BASEPATH') OR exit('No direct script access allowed');

//kelas welcome dengan mengextends class dari CI yakni ci controller
class Welcome extends CI_Controller {

	//fungsi yang bisa digunakan dalam satu kelas, cukup menuliskan di sini maka semua dalam satu kelas sudah bisa dipanggil
	function __construct(){
		//dijadikan sebagai parent class
		parent::__construct();

		//memanggil model dengan nama model_stki
		$this->load->model('model_stki');
	}

	//fungsi index (yang jadi default ketika class ditampilkan)
	public function index()
	{
		//variable data dengan array dokumen yang berisi data yang diambil dari fungsi yang ada di class model
		$data['dokumen'] = $this->model_stki->view_all();

		//menampilkan class view dengan data array diincludekan
		$this->load->view('dashboard', $data);
	}

	//fungsi untuk import
	public function import(){

		//menempatkan dimana file akan diimport
		$config['upload_path'] = './imports/';

		//menentukan tipe file apa yang bisa diimport
		$config['allowed_types'] = 'doc|pdf';

		//menentukan batas maksimal ukuran file
		$config['max-size'] = '5000';

		//memanggil library upload di ci untuk proses import
		$this->load->library('upload', $config);

		//pengecekan kondisi jika file gagal diupload
		if(! $this->upload->do_upload('userfile')){

			//variable data dengan array dokumen yang berisi data yang diambil dari fungsi yang ada di class model
			$data['dokumen'] = $this->model_stki->view_all();

			//menampilkan class view dengan data array diincludekan
			$this->load->view('dashboard', $data);

		//jika file berhasil diimport
		} else {

			//memanggil data file yang sudah tersimpan di class upload (bawaan ci)
			$this->upload->data('userfile');

			//mendefinisikan nama file yang di import
			$tes = $this->upload->data('file_name');

			//pengecekan kondisi jika file yang diimport berupa file doc
			if($this->upload->data('file_ext') == '.doc'){

				//mendefinisikan letak path (direktori file)
				$fileupload = $this->upload->data('full_path');

				//$path untuk memnaggil directory library antiword
				$path = 'c:/xampp/htdocs/tugas-stki/antiword/';

				//file txt sementara untuk menyimpan dokumen hasil parse
				$filetxt = 'data.txt';

				//melakukan eksekusi variable path dengan menggunakan antiword dan disimpan dalam format txt
				exec($path.'antiword.exe -m cp850.txt '.$fileupload.' > '.$filetxt);

				//untuk membuka file hasil parse
				$file = fopen($filetxt, 'r');

				//menutup file hasil parse
				fclose($file);

				//array untuk menyimpan data dokumen
				$isi = array(

					//nama mengambil isi dari library upload yakni nama file
					'nama' => $this->upload->data('file_name'),

					//size mengambil isi dari library upload yakni  ukuran file 
					'size' => $this->upload->data('file_size'),

					//dokumen berisi hasil get (ambil) hasil parse dokumen
					'dokumen' => file_get_contents($filetxt)
					);

				//memanggil model dengan fungsi add yang berisi array
				$this->model_stki->add($isi);

				//kembali lagi ke index
				redirect(base_url());

			//jika file import berupa file pdf
			} else if ($this->upload->data('file_ext') == '.pdf'){

				//mengincludekan library dari composer
				include 'vendor/autoload.php';

				//memanggil fungsi parser dari library pdf parser
				$parser = new \Smalot\PdfParser\Parser();

				//memparse file pdf dengan menggunakan path / direktory
				$pdf    = $parser->parseFile($this->upload->data('full_path'));

				//mendapatkan hasil parse file pdf
				$text = $pdf->getText();

				//membuat array untuk menyimpan data
				$isi = array(

					//nama mengambil isi dari library upload yakni nama file
					'nama' => $this->upload->data('file_name'),

					//size mengambil isi dari library upload yakni  ukuran file 
					'size' => $this->upload->data('file_size'),

					//dokumen berisi hasil get (ambil) hasil parse dokumen
					'dokumen' => $text
					);

				//memanggil model dengan fungsi add yang berisi array
				$this->model_stki->add($isi);

				//kembali lagi ke index
				redirect(base_url());

			}
		}
	}

	//fungsi view dengan parameter id untuk menampilkan detail sesuai id
	public function view($id){

		//memanggil model dengan fungsi lihat berparameter id
		$lihat['lihat'] = $this->model_stki->lihat($id);

		//menampilkan hasil pemanggilan dari model kedalam class view
		$this->load->view('view', $lihat);
	}

	//fungsi untuk menampilkan dokumen untuk ditoken
	public function token($id){

		//memanggil fungsi tokenisasi dari model stki
		$tok['tkn'] = $this->model_stki->tokenisasi($id);

		//memanggil view bernama token dengan parameter dari array tok
		$this->load->view('token',$tok);
	}

	//fungsi untuk menyimpan hasil token
	public function simpan_token(){
		$id = $this->input->post('id');
		$kata = $this->input->post('huruf');
		$angka = $this->input->post('angka');
		$char = $this->input->post('karakter');

		$token = array(
			'kata' => $kata,
			'angka' => $angka,
			'karakter' => $char 
			);

		$where = array('id' => $id);

		$tes = $this->model_stki->update_token($where, $token);
		if(! $tes){
			$this->session->set_flashdata('notif', "<div class='alert alert-info'>SIMPAN TOKEN BERHASIL</div>");
			redirect(base_url());
		} else {
			echo "tokenisasi gagal menyimpan";
		}
	}

	public function hapus($id){
		$this->model_stki->hapus($id);
		$this->session->set_flashdata('notif', "<div class='alert alert-danger'>HAPUS BERHASIL</div>");
		redirect(base_url());
	}

	public function edit($id){
		$ed['ubah'] = $this->model_stki->edit($id);
		$this->load->view('edit', $ed);
	}

	public function update(){
		$id = $this->input->post('id');
		$nama = $this->input->post('nama');
		$doc = $this->input->post('dokumen');

		$ubah_doc = array(
			'nama' => $nama,
			'dokumen' => $doc
			);

		$where = array('id', $id);

		$tes = $this->model_stki->ubah($where, $ubah_doc);
		if(! $tes){
			$this->session->set_flashdata('notif', "<div class='alert alert-info'>EDIT BERHASIL</div>");
			redirect(base_url());
		} else {
			echo "gagal menyimpan";
		}
	}

	public function hasil_token(){
		$lihat['lihat'] = $this->model_stki->lihat_token();
		$this->load->view('hasil_token', $lihat);
	}

	public function detail_token($id){
		$tok['tkn'] = $this->model_stki->detail_token($id);
		$this->load->view('detail_token',$tok);
	}

	public function stop($id){
		$stop['stopword'] = $this->model_stki->stopword($id);
		$this->load->view('stopword', $stop);
	}

	public function steam($id){
		$cek = $this->model_stki->cek_token($id);
		$data1 = $cek->kata_stopword;

		$data = strtolower($data1);

		$id = $cek->id;
		//echo "Kata dasar : ".$pros_stem.'<br/>';

		$tokenKarakter=array('?','?',' ','/',',','?','.',':',';',',','!','[',']','{','}','(',')','-','_','+','=','<','>','\'','"','\\','@','#','$','%','^','&','*','`','~','0','1','2','3','4','5','6','7','8','9','â?','?','?');
		$teks= str_replace($tokenKarakter,' ',$data);
		$tok = strtok($teks, "\n\t");

		$this->load->view('template/head');

		echo "<p>&nbsp;<br>&nbsp;<p>
				<div class='container'>
				<h3>Hasil Stemming</h3>
		";

		echo "<b>".$cek->nama."</b><br/>";

		echo anchor('welcome/hasil_token','KEMBALI',['class'=>'btn btn-sm btn-info']);

		echo "<hr/>";

		echo form_open('welcome/simpan_stemming');

		echo "<input type='text' name='id' value='$id' hidden>";

		echo "<textarea class='form-control' cols='40' rows='15' name='steam' readonly>";

		$split = explode(' ',$teks);
		foreach($split as $key=>$kata){
			$hasil = $this->stemming(trim($kata)).' ;';

			$teks = str_replace(";", "", $hasil);

			print_r($teks);

		}	
		echo "</textarea><br/>";
		echo "<center><input class='btn btn-sm btn-danger' type='submit' value='SIMPAN'></center></div>";
	}

	public function simpan_stemming(){
		$id = $this->input->post('id');
		
		$kirim = array(
			'kata_stemming' => $this->input->post('steam')
			);

		$proses = $this->model_stki->simpan_stemming($id, $kirim);

		if(! $proses){
			$this->session->set_flashdata('notif', "<div class='alert alert-success'>STEMMING BERHASIL, ".anchor('welcome/hasil_stem','LIHAT')."</div>");
			redirect('welcome/hasil_token');
		}

	}

	public function hasil_stem(){
		$stem['stemm'] = $this->model_stki->hasil_stem();
		$this->load->view('hasil_stem', $stem);
	}

	public function detail_stem($id){
		$hasil['stem'] = $this->model_stki->detail_stem($id);
		$this->load->view('detail_stem', $hasil);
	}

	public function indexing($id){
		$hasil['stem'] = $this->model_stki->detail_stem($id);
		$this->load->view('indexing', $hasil);
	}

	public function simpan_index(){
		$id = $this->input->post('id');
		$kata_index = $this->input->post('kata_index');

		$simpan = array(
			'kata_indexing_1' => $kata_index
			);

		$proses = $this->model_stki->simpan_index($id, $simpan);

		$this->session->set_flashdata('notif', "<div class='alert alert-success'>SIMPAN TERM BERHASIL</div>");
		redirect('welcome/hasil_stem');

	}

	public function cekKamus($kata){ 
		$sql = $this->model_stki->cekKamus($kata);

		if($sql==true){
			return true;
		} else {
			return false;
		}
	}

	function Del_Inflection_Suffixes($kata){ 
	$kataAsal = $kata;
	
	if(preg_match('/([km]u|nya|[kl]ah|pun)\z/i',$kata)){ // Cek Inflection Suffixes
		$__kata = preg_replace('/([km]u|nya|[kl]ah|pun)\z/i','',$kata);

		return $__kata;
	}
	return $kataAsal;
}

// Cek Prefix Disallowed Sufixes (Kombinasi Awalan dan Akhiran yang tidak diizinkan)
	function Cek_Prefix_Disallowed_Sufixes($kata){

	if(preg_match('/^(be)[[:alpha:]]+/(i)\z/i',$kata)){ // be- dan -i
		return true;
	}

	if(preg_match('/^(se)[[:alpha:]]+/(i|kan)\z/i',$kata)){ // se- dan -i,-kan
		return true;
	}
	
	if(preg_match('/^(di)[[:alpha:]]+/(an)\z/i',$kata)){ // di- dan -an
		return true;
	}
	
	if(preg_match('/^(me)[[:alpha:]]+/(an)\z/i',$kata)){ // me- dan -an
		return true;
	}
	
	if(preg_match('/^(ke)[[:alpha:]]+/(i|kan)\z/i',$kata)){ // ke- dan -i,-kan
		return true;
	}
	return false;
}

// Hapus Derivation Suffixes ("-i", "-an" atau "-kan")
function Del_Derivation_Suffixes($kata){
	$kataAsal = $kata;
	if(preg_match('/(i|an)\z/i',$kata)){ // Cek Suffixes
		$__kata = preg_replace('/(i|an)\z/i','',$kata);
		if($this->cekKamus($__kata)){ // Cek Kamus
			return $__kata;
		}else if(preg_match('/(kan)\z/i',$kata)){
			$__kata = preg_replace('/(kan)\z/i','',$kata);
			if($this->cekKamus($__kata)){
				return $__kata;
			}
		}
/*– Jika Tidak ditemukan di kamus –*/
	}
	return $kataAsal;
}

// Hapus Derivation Prefix ("di-", "ke-", "se-", "te-", "be-", "me-", atau "pe-")
function Del_Derivation_Prefix($kata){
	$kataAsal = $kata;

	/* —— Tentukan Tipe Awalan ————*/
	if(preg_match('/^(di|[ks]e)/',$kata)){ // Jika di-,ke-,se-
		$__kata = preg_replace('/^(di|[ks]e)/','',$kata);
		
		if($this->cekKamus($__kata)){
			return $__kata;
		}
		
		$__kata__ = $this->Del_Derivation_Suffixes($__kata);
			
		if($this->cekKamus($__kata__)){
			return $__kata__;
		}
		
		if(preg_match('/^(diper)/',$kata)){ //diper-
			$__kata = preg_replace('/^(diper)/','',$kata);
			$__kata__ = $this->Del_Derivation_Suffixes($__kata);
		
			if($this->cekKamus($__kata__)){
				return $__kata__;
			}
			
		}
		
		if(preg_match('/^(ke[bt]er)/',$kata)){  //keber- dan keter-
			$__kata = preg_replace('/^(ke[bt]er)/','',$kata);
			$__kata__ = $this->Del_Derivation_Suffixes($__kata);
		
			if($this->cekKamus($__kata__)){
				return $__kata__;
			}
		}
			
	}
	
	if(preg_match('/^([bt]e)/',$kata)){ //Jika awalannya adalah "te-","ter-", "be-","ber-"
		
		$__kata = preg_replace('/^([bt]e)/','',$kata);
		if($this->cekKamus($__kata)){
			return $__kata; // Jika ada balik
		}
		
		$__kata = preg_replace('/^([bt]e[lr])/','',$kata);	
		if($this->cekKamus($__kata)){
			return $__kata; // Jika ada balik
		}	
		
		$__kata__ = $this->Del_Derivation_Suffixes($__kata);
		if($this->cekKamus($__kata__)){
			return $__kata__;
		}
	}
	
	if(preg_match('/^([mp]e)/',$kata)){
		$__kata = preg_replace('/^([mp]e)/','',$kata);
		if($this->cekKamus($__kata)){
			return $__kata; // Jika ada balik
		}
		$__kata__ = $this->Del_Derivation_Suffixes($__kata);
		if($this->cekKamus($__kata__)){
			return $__kata__;
		}
		
		if(preg_match('/^(memper)/',$kata)){
			$__kata = preg_replace('/^(memper)/','',$kata);
			if($this->cekKamus($kata)){
				return $__kata;
			}
			$__kata__ = $this->Del_Derivation_Suffixes($__kata);
			if($this->cekKamus($__kata__)){
				return $__kata__;
			}
		}
		
		if(preg_match('/^([mp]eng)/',$kata)){
			$__kata = preg_replace('/^([mp]eng)/','',$kata);
			if($this->cekKamus($__kata)){
				return $__kata; // Jika ada balik
			}
			$__kata__ = $this->Del_Derivation_Suffixes($__kata);
			if($this->cekKamus($__kata__)){
				return $__kata__;
			}
			
			$__kata = preg_replace('/^([mp]eng)/','k',$kata);
			if($this->cekKamus($__kata)){
				return $__kata; // Jika ada balik
			}
			$__kata__ = $this->Del_Derivation_Suffixes($__kata);
			if($this->cekKamus($__kata__)){
				return $__kata__;
			}
		}
		
		if(preg_match('/^([mp]eny)/',$kata)){
			$__kata = preg_replace('/^([mp]eny)/','s',$kata);
			if($this->cekKamus($__kata)){
				return $__kata; // Jika ada balik
			}
			$__kata__ = $this->Del_Derivation_Suffixes($__kata);
			if($this->cekKamus($__kata__)){
				return $__kata__;
			}
		}
		
		if(preg_match('/^([mp]e[lr])/',$kata)){
			$__kata = preg_replace('/^([mp]e[lr])/','',$kata);
			if($this->cekKamus($__kata)){
				return $__kata; // Jika ada balik
			}
			$__kata__ = $this->Del_Derivation_Suffixes($__kata);
			if($this->cekKamus($__kata__)){
				return $__kata__;
			}
		}
		
		if(preg_match('/^([mp]en)/',$kata)){
			$__kata = preg_replace('/^([mp]en)/','t',$kata);
			if($this->cekKamus($__kata)){
				return $__kata; // Jika ada balik
			}
			$__kata__ = $this->Del_Derivation_Suffixes($__kata);
			if($this->cekKamus($__kata__)){
				return $__kata__;
			}
			
			$__kata = preg_replace('/^([mp]en)/','',$kata);
			if($this->cekKamus($__kata)){
				return $__kata; // Jika ada balik
			}
			$__kata__ = $this->Del_Derivation_Suffixes($__kata);
			if($this->cekKamus($__kata__)){
				return $__kata__;
			}
		}
			
		if(preg_match('/^([mp]em)/',$kata)){
			$__kata = preg_replace('/^([mp]em)/','',$kata);
			if($this->cekKamus($__kata)){
				return $__kata; // Jika ada balik
			}
			$__kata__ = $this->Del_Derivation_Suffixes($__kata);
			if($this->cekKamus($__kata__)){
				return $__kata__;
			}
			
			$__kata = preg_replace('/^([mp]em)/','p',$kata);
			if($this->cekKamus($__kata)){
				return $__kata; // Jika ada balik
			}
			
			$__kata__ = $this->Del_Derivation_Suffixes($__kata);
			if($this->cekKamus($__kata__)){
				return $__kata__;
			}
		}	
	}
	return $kataAsal;
}

public function stemming($kata){ 
		$kataAsal = $kata;

		$cekKata = $this->cekKamus($kata);
		if($cekKata == true){ // Cek Kamus
			return $kata; // Jika Ada maka kata tersebut adalah kata dasar
			}else{ //jika tidak ada dalam kamus maka dilakukan stemming
		
			$kata = $this->Del_Inflection_Suffixes($kata);
			if($this->cekKamus($kata)){
				return $kata;
			}
		
			$kata = $this->Del_Derivation_Suffixes($kata);
			if($this->cekKamus($kata)){
				return $kata;
			}
		
			$kata = $this->Del_Derivation_Prefix($kata);
			if($this->cekKamus($kata)){
				return $kata;
			}
		}
	}

	public function simpan_stopword(){
		$id = $this->input->post('id');
		$kata = $this->input->post('stop');

		$simpan_stop = array(
			'kata_stopword' => $kata
			);

		$this->model_stki->simpan_stopword($id, $simpan_stop);
		$this->session->set_flashdata('notif', "<div class='alert alert-info'>SIMPAN FILTER BERHASIL</div>");
		redirect('welcome/hasil_token');
	}
	

}
