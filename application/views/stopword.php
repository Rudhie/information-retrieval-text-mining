<?php $this->load->view('template/head'); ?>&nbsp;

<div class="container">
	<h3>Hasil Filter (Stopword)</h3>
	<?php echo anchor('welcome/hasil_token','KEMBALI',['class'=>'btn btn-sm btn-info']); ?>
	<?php echo form_open('welcome/simpan_stopword',['class'=>'form-horizontal']); ?>
	<?php 
		foreach ($stopword as $value) {
			echo "<b>".$value['nama']."</b>";
	?>
		<input type="hidden" class="form-control" name="id" value="<?php echo $value['id']; ?>">
		<?php

		$teks = $value['kata'];

		$teks = strtolower($teks);

		$stop = $this->db->get('stopword');
		foreach ($stop->result_array() as $stop) {
			$astoplist = array (" ".$stop['kata']." ");
			// $astoplist = array (" akan ","yang","di", "juga", "dari", "dia", "kami", "kamu", "ini", "itu", 
   //                          "atau","oleh", "dan", "tersebut", "pada", "dengan", "adalah", "yaitu");
		

		foreach ($astoplist as $i => $value) {
			$teks = str_replace($astoplist[$i], "", $teks);
			$teks = str_replace("[]", "", $teks);
		}
	}

		$teks = trim($teks);

		?>

		<textarea class="form-control" name="stop" cols="160" rows="20" readonly><?php echo $teks; ?></textarea>
	<?php } ?>
	&nbsp;<p>
	<input type="submit" name="tambah" class="btn btn-sm btn-danger" value="SIMPAN">
	<?php form_close(); ?>
</div>
<?php $this->load->view('template/foot'); ?>