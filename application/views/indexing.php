<?php $this->load->view('template/head'); ?>&nbsp;

<div class="container">
	<h3>Hasil Indexing</h3>
	<?php echo anchor('welcome/hasil_stem','KEMBALI',['class'=>'btn btn-sm btn-info']); ?>
	<?php echo form_open('welcome/simpan_index',['class'=>'form-horizontal']); ?>
	<hr/>
	<?php foreach ($stem as $value) { ?>
		<input type="hidden" class="form-control" name="id" value="<?php echo $value->id; ?>">
		<?php
			$kalimat = $value->kata_stemming;
			$kalimat = str_replace(';', ' ', $kalimat);
			$kalimat = array_unique(explode(' ', str_replace('[]', '', $kalimat)));
			$kalimat = implode($kalimat,' ');
		?>
		<textarea class="form-control" name="kata_index" cols="160" rows="20" readonly><?php print_r($kalimat); ?></textarea>
	<?php } ?>
	&nbsp;<p></p>
	<center>
	<input type="submit" name="tambah" class="btn btn-sm btn-danger" value="SIMPAN">
	</center>
	<?php form_close(); ?>
</div>
<?php $this->load->view('template/foot'); ?>