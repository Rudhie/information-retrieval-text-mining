<?php

defined('BASEPATH') OR exit('No direct script access allowed');

$this->load->view('template/head');

?>
&nbsp;
<div class="container">
	<h3>Hasil Tokenisasi</h3>
	<?php echo anchor(base_url(),'KEMBALI',['class'=>'btn btn-sm btn-info']); ?><hr/>

	<?php echo form_open('welcome/simpan_token',['method'=>'post']); ?>
	
<?php
	
	foreach ($tkn as $value) {	?>
		<input type="hidden" name="id" value="<?php echo $value->id ?>">
		
		<?php 
		$str = $value->dokumen;
		$token = strtok($str, " ");?>
		<textarea name="huruf" class="form-control" cols="180" rows="40" readonly>
		<?php while ($token !== false) {
			
			$token = strtok(" ");
			
			if(preg_match("/^[a-zA-Z]+$/", $token) == 1) {
				$text = $token;
				echo "[ ".$text." ] ";
			}
					
		} ?>
		</textarea>
		
		<?php echo "<hr/>";
		
		$str = $value->dokumen;
		$token = strtok($str, " ");?>
		<textarea name="angka" class="form-control" cols="180" readonly>
		<?php while ($token !== false) {
			
			$token = strtok(" ");
			
			if(preg_match("/^[0-9]+$/", $token) == 1) {
				$angka = $token;
				echo "".$angka."; "; 
			}
					
		} ?>
		</textarea>
		<?php echo "<hr/>";
		
		$str = $value->dokumen;
		$token = strtok($str, " "); ?>
		<textarea name="karakter" class="form-control" cols="180" readonly>
		<?php while ($token !== false) {
			
			$token = strtok(" ");
			
			if(preg_match("/^[.,()<>!@#$%^&*`~?.,';:{}]+$/", $token) == 1) {
				$char = $token;
				echo "".$char." ";
			}
					
		} ?>
		</textarea>
		
	<?php }
?>


<hr/>
	<center><input type="submit" value="SIMPAN" class="btn btn-sm btn-danger"></center><br>&nbsp;
	<?php form_close(); ?>
</div>
<?php $this->load->view('template/foot'); ?>