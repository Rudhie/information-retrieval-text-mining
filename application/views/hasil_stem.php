<?php

defined('BASEPATH') OR exit('No direct script access allowed');

$this->load->view('template/head');
?>
<p>&nbsp;<br>&nbsp;<p>
<div class="container">
	<div  class="row">
		<div class="col-md-12">
			<center>
		        <strong><?php echo $this->session->flashdata('notif'); ?></strong>
			</center>
			<h4>Stemming</h4><hr/>
			<table class="table table-hover">
				<thead style="background-color:#ff0000;color:white">
					<tr>
						<td>No.</td>
						<td>Nama File</td>
						<td>Simple Stem</td>
						<td>Action</td>
					</tr>
				</thead>
				<tbody>
					<?php $a=0; foreach ($stemm as $value) { ?>
					<?php if($value->kata_stemming != '') { ?>
						<tr>
							<td><?php $a++; echo $a; ?></td>
							<td><?php echo $value->nama; ?></td>
							<td><?php echo substr($value->kata_stemming, 0, 60); ?></td>
							<td><?php echo anchor('welcome/detail_stem/'.$value->id, 'VIEW', ['class'=>'btn btn-sm btn-info']); ?>&nbsp;
								<?php echo anchor('welcome/indexing/'.$value->id, 'INDEXING',['class'=>'btn btn-sm btn-success']); ?>
							</td>
						</tr>
					<?php } } ?>
				</tbody>
			</table>
		</div>
	</div>
</div>
<?php $this->load->view('template/foot'); ?>