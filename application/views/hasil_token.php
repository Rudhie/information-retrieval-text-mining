<?php

defined('BASEPATH') OR exit('No direct script access allowed');

$this->load->view('template/head');
?>
<p>&nbsp;<br>&nbsp;<p>
<div class="container">
	<div  class="row">
		<div class="col-md-12">
			<center>
		        <strong><?php echo $this->session->flashdata('notif'); ?></strong>
			</center>
			<h4>Tokenisasi</h4><hr/>
			<table class="table table-hover">
				<thead style="background-color:#ff0000;color:white">
					<tr>
						<td>No.</td>
						<td>Nama File</td>
						<td>Simple Token</td>
						<td>Action</td>
					</tr>
				</thead>
				<tbody>
					<?php $a=0; foreach ($lihat as $doc) { ?>
					<?php if ($doc->kata != ''){ ?>
						<tr>
							<td><?php $a++; echo $a; ?></td>
							<td><?php echo $doc->nama; ?></td>
							<td><?php echo substr($doc->kata, 0, 60); ?></td>
							<td><?php echo anchor('welcome/detail_token/'.$doc->id, 'VIEW', ['class'=>'btn btn-sm btn-info']); ?>&nbsp;

							<?php echo anchor('welcome/stop/'.$doc->id, 'STOPWORD', ['class'=>'btn btn-sm btn-success']); ?>

							<?php if($doc->kata_stopword != ''){ ?>

							<?php echo anchor('welcome/steam/'.$doc->id, 'STEMMING', ['class'=>'btn btn-sm btn-warning']); ?>
							<?php } else { }

							?>
							</td>
						</tr>
					<?php } } ?>
				</tbody>
			</table>
		</div>
	</div>
</div>
<?php $this->load->view('template/foot'); ?>