<!DOCTYPE html>
<html>
<head>
	<title>Information Retreival</title>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/bootstrap.min.css'); ?>">
</head>
<body>
<nav class="navbar navbar-inverse navbar-fixed-top">
  <div class="container">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <?php echo anchor('welcome','Information Retreival',['class'=>'navbar-brand']) ?>
    </div>
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav navbar-right">
        <li><?php echo anchor('welcome','Dashboard');?></li>
        <li><?php echo anchor('welcome/hasil_token','Token');?></li>
        <li><?php echo anchor('welcome/hasil_stem','Stem');?></li>
        <li><?php echo anchor('bobot','Weighting');?></li>
      </ul>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>
<br>
